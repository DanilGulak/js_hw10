const eyeIcon = document.querySelectorAll('.icon-password');

eyeIcon.forEach( (element) => {
    element.addEventListener('click', () => {
        if (element.classList.contains('fa-eye')) {
            element.previousElementSibling.type = 'text';
            element.classList.remove('fa-eye');
            element.classList.add('fa-eye-slash');
        } else {
            element.previousElementSibling.type = 'password';
            element.classList.remove('fa-eye-slash');
            element.classList.add('fa-eye');
        }
    })
})

const validationBtn = document.querySelector('.btn');
const passwordInput = document.querySelectorAll('input');

let error = document.createElement('span');
error.textContent = 'Нужно ввести одинаковые значения!';
error.style.color = 'red';

validationBtn.addEventListener('click', (event) => {
    event.preventDefault();

    if (passwordInput[0].value === passwordInput[1].value) {
        alert('You are welcome!');
        passwordInput.forEach( (element) => {
            element.value = '';
            error.remove();
        })
    } else {
        passwordInput[1].parentElement.append(error);
    }
})



